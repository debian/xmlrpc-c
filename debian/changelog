xmlrpc-c (1.59.03-7) unstable; urgency=medium

  * Rewrite d/copyright in machine-parsable format

 -- Alexandre Detiste <tchet@debian.org>  Sun, 16 Mar 2025 23:03:22 +0100

xmlrpc-c (1.59.03-6) unstable; urgency=medium

  [ Yegor Yefremov ]
  * Backport pkg-config patch from 1.6x.x branch (Closes: #1086167)

 -- Alexandre Detiste <tchet@debian.org>  Fri, 08 Nov 2024 13:15:48 +0100

xmlrpc-c (1.59.03-5) unstable; urgency=medium

  [ Helmut Grohne ]
  * Fix FTCBFS: Update variable name for build architecture compiler.
    (Closes: #1084809)

 -- Alexandre Detiste <tchet@debian.org>  Wed, 16 Oct 2024 18:19:19 +0200

xmlrpc-c (1.59.03-4) unstable; urgency=medium

  * Add a the libxmlrpc-core-c3-dev -> libxmlrpc-util-dev
    dependency (Closes: #1072163)

 -- Alexandre Detiste <tchet@debian.org>  Fri, 31 May 2024 12:37:13 +0200

xmlrpc-c (1.59.03-3) unstable; urgency=medium

  * add 'Replaces:' (Closes: #1071735)

 -- Alexandre Detiste <tchet@debian.org>  Sat, 25 May 2024 17:47:54 +0200

xmlrpc-c (1.59.03-2) unstable; urgency=medium

  * Split-out xmlrpc-utils4 (Closes: #1071465)
  * Apply Multi-Arch hint
  * Close old Ubuntu bugs: (LP: #464509, LP: #1865344)

 -- Alexandre Detiste <tchet@debian.org>  Wed, 22 May 2024 10:14:45 +0200

xmlrpc-c (1.59.03-1) unstable; urgency=medium

  * Adopt package (Closes: #773435)
  * New upstream version 1.59.03 (Closes: #524550)
  * Switch to debhelper compat 13
  * Refresh patches
  * Bump soname for C++ lib, no reverse dep in Debian
  * Merge remaining time64 changes for C lib from the NMU
  * Provide .pc files for pkg-config (Closes: #614423)
  * LTO does work now (Closes: #1015709)
  * Pull in curl development package when installing
    xmlrpc devevelopement (Closes: #928702)

  [ Jörg Frings-Fürst ]
  * Work for new release
  * Imported Upstream version 1.33.15+svn20141226~2672
  * Imported Upstream version 1.33.17

 -- Alexandre Detiste <tchet@debian.org>  Fri, 10 May 2024 18:40:43 +0200

xmlrpc-c (1.33.14-12) unstable; urgency=medium

  * QA upload.
  * Fix extraneous X-Time64-Compat declarations.

 -- Steve Langasek <vorlon@debian.org>  Sun, 03 Mar 2024 21:26:20 +0000

xmlrpc-c (1.33.14-11.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Rename libraries for 64-bit time_t transition.  Closes: #1063279

 -- Steve Langasek <vorlon@debian.org>  Thu, 29 Feb 2024 07:39:42 +0000

xmlrpc-c (1.33.14-11) unstable; urgency=medium

  * QA upload.

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster (oldstable)

 -- Jelmer Vernooĳ <jelmer@debian.org>  Thu, 13 Oct 2022 13:27:40 +0100

xmlrpc-c (1.33.14-10) unstable; urgency=medium

  * QA upload.

  [ Debian Janitor ]
  * Remove constraints unnecessary since stretch:
    + Build-Depends: Drop versioned constraint on debhelper.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sun, 22 Aug 2021 03:19:47 +0100

xmlrpc-c (1.33.14-9) unstable; urgency=medium

  * QA upload.
  * debian/control:
       - Added 'Rules-Requires-Root: no' to source stanza.
       - Bumped Standards Version to 4.5.0.
       - Changed VCS fields to Salsa.
  * debian/gbp.conf: changed master branch to debian/master.
  * debian/libxmlrpc-core-c3-dev.manpages: created to install manpage.
  * debian/manpage/*: created to add a manpage to xmlrpc.
  * debian/salsa-ci.yml: created to perform CI in Salsa.
  * debian/watch: updated. Wasn't working before.

 -- Carlos Henrique Lima Melara <charlesmelara@outlook.com>  Fri, 19 Jun 2020 18:31:22 -0300

xmlrpc-c (1.33.14-8) unstable; urgency=medium

  * QA upload.
  * Get rid of the recently added udeb package.  Closes: #874268
    It turned out that it has been broken already in Ubuntu several years
    ago already (with libcurl dropping its udeb), and nobody ever noticed
    it.  Now it's also broken in Debian as well…
    So, it's clear that this udeb isn't actually serving any good purpose.

 -- Mattia Rizzolo <mattia@debian.org>  Mon, 04 Sep 2017 22:20:42 +0200

xmlrpc-c (1.33.14-7) unstable; urgency=medium

  * QA upload.
  * Upload to unstable.
  * debian/copyright: clarify a copyright/license statement.  Closes: #857456
  * Bump shlibs version of libxmlrpc-c++8v5 due to GCC-7 op mangling.
    Closes: #871290
  * debian/rules:
    + Don't explicitly use quilt, source format '3.0 (quilt)' implies it.
    + Stop manual update of config.{sub,guess}, nowadays
      dh_update_autotools_config takes care of it automatically.
    + Prefer `$(MAKE) -C tools` over `cd tools && make`
    + Leave the job of calling `make check` to dh_auto_test.
    + Move this removal due to license issues from clean (where it might be
      skipped) to configure.
    + Clean up the cleaning code.
    + Fix FTCBFS by passing BUILDTOOL_CC and BUILDTOOL_CCLD to make.
      Thanks to Helmut Grohne <helmut@subdivi.de> for the patch
      Closes: #874183
  * debian/control:
    + Bump Standards-Version to 4.1.0, no changes needed.
    + Don't start short description with an article
    + Drop build-dependency on quilt.
    + Run wrap-and-sort.

 -- Mattia Rizzolo <mattia@debian.org>  Mon, 04 Sep 2017 11:47:11 +0200

xmlrpc-c (1.33.14-6) experimental; urgency=medium

  * QA upload.
  * Add patch from Chris Lamb to make the build reproducible.  Closes: #860279

 -- Mattia Rizzolo <mattia@debian.org>  Sat, 15 Apr 2017 00:00:19 +0200

xmlrpc-c (1.33.14-5) experimental; urgency=medium

  * QA upload.
  * Drop README.source, how a quilt package is handled is common knowledge now.
  * Drop the lintian override, it's a pedantic tag, I also doubt it's a FPO.
  * Add a udeb for libxmlrpc-core-c3.
    Change imported from Ubuntu.  See  LP: #831496 for the origin.
  * Move XmlRpcCpp.h from libxmlrpc-core-c3-dev to libxmlrpc-c++8-dev, as it's
    c++ stuff.  Closes: #644161

 -- Mattia Rizzolo <mattia@debian.org>  Sat, 11 Mar 2017 00:38:27 +0100

xmlrpc-c (1.33.14-4) unstable; urgency=medium

  * QA upload.
  * Add patch fixing FTBFS on non-x86 architectures.

 -- Mattia Rizzolo <mattia@debian.org>  Fri, 10 Mar 2017 23:41:13 +0100

xmlrpc-c (1.33.14-3) unstable; urgency=medium

  * Upload to unstable. QA upload.
  * debian/libxmlrpc-core-c3-dev.doc-base created.
  * debian/libxmkrpc-c++8v5.install:
      - debian/lintian/libxmlrpc-c++8 entry removed.
      - debian/lintian/libxmlrpc-c++8v5 entry created.
  * debian/libxmlrpc-core-c3.install:
      - debian/lintian/libxmlrpc-core-c3 entry removed.
  * debian/lintian:
      - libxmlrpc-core-c3 removed.
      - libxmlrpc-c++8 removed.
      - libxmlrpc-c++8v5 created. (about hardening)
  * debian/source/format:
      3.0 (quilt)
  * debian/patches:
      - updated (blhc --all quiet now):
          - XXXFLAGS.patch
          - ldflag_lib_expat_gennmtab.patch
      - typo.patch created.
  * debian/watch:
      - with 'debian uupdate'.

 -- Herbert Parentes Fortes Neto <hpfn@debian.org>  Sat, 23 Jul 2016 13:10:10 -0300

xmlrpc-c (1.33.14-2) experimental; urgency=medium

  * QA upload.
  * debian/clean file created:
      - rm src/cpp/{blddir, srcdir}
  * debian/control:
      - bump Standards-Version from 3.9.6 to 3.9.8.
      - Vcs-* fields fixed.
      - Section entry from Package's statement removed. It duplicates the
        Section entry from Source's statement.
        from Source's statement.
  * debian/rules:
      - '-Wl,--as-needed' added to LDFLAGS.
  * debian/patches:
      - no_curl_test.patch to avoid testclient.cpp. FTBFS. (Closes: #831261)
      - ldflag_lib_expat_gennmtab.patch. hardening.
      - XXXFLAGS.patch. adds some hardening.
        (still 'hardening-no-pie' lintian)
  * debian/watch created.

 -- Herbert Parentes Fortes Neto <hpfn@debian.org>  Sat, 23 Jul 2016 10:31:02 -0300

xmlrpc-c (1.33.14-1) unstable; urgency=medium

  * QA upload.
  * Maintainer field set to QA Group.
  * Bump Standards-Version to 3.9.6.
  * Update FSF address in debian/copyright.

 -- Emanuele Rocca <ema@debian.org>  Sun, 13 Dec 2015 14:06:51 +0100

xmlrpc-c (1.33.14-0.3) unstable; urgency=medium

  * Non-maintainer upload.
  * Rename library packages for g++ 5 transition (see #791313)

 -- Jonathan Wiltshire <jmw@debian.org>  Tue, 11 Aug 2015 22:31:41 +0100

xmlrpc-c (1.33.14-0.2) unstable; urgency=medium

  * Non-maintainer upload.
  * Document missing license (Closes: #760377)
  * Clean unused code to prevent conflicting licenses
  * Add ${shlibs:Depends} to depends of xmlrpc-api-utils to prevent
    several missing dependencies (Thanks lintian)

 -- Paul Gevers <elbrus@debian.org>  Sat, 08 Nov 2014 20:53:51 +0100

xmlrpc-c (1.33.14-0.1) unstable; urgency=low

  * Non-maintainer upload.
  * New upstream release. (Closes: #660213)
  * Drop upstream patches, refresh others.
  * gbp.conf: Use upstream branch as the tree, and enable
    pristine-tar-commit.
  * control, *.install: Bump the C++ lib soname, update C lib minor.
  * xmlrpc-api-utils.*: Fix xml-rpc-api2txt paths.
  * control: Rewrap *Depends for readability.
  * compat, control, rules, *.install: Bump compat level to 9, migrate
    to dh, and fix the install paths.
  * fix-format-security.diff: Fix a build error with default buildflags.
  * control: Drop libxmlrpc-c3-dev and obsolete
    Provides/Conflicts/Replaces.
  * c3-dev.install: Install xmlrpc.html to the correct path.
  * control: Bump policy to 3.9.5, no changes.
  * control: Add Vcs entries.
  * control: Make libxmlrpc-c++8-dev conflict with the old pkg.
  * control: Add libxmlrpc-c++8 to xmlrpc-api-utils Depends, xml-rcp-
    api2cpp needs it.
  * Drop source.lintian-overrides, unneeded.

 -- Timo Aaltonen <tjaalton@debian.org>  Mon, 01 Sep 2014 16:30:13 +0300

xmlrpc-c (1.16.33-3.2) unstable; urgency=medium

  * Non-maintainer upload.
  * Fix CVE-2012-0876 and CVE-2012-1148 in embedded Expat copy. Thanks to
    Tyler Hicks for the patch and the report (Closes: #687672)

 -- Moritz Muehlenhoff <jmm@debian.org>  Wed, 03 Oct 2012 12:09:04 +0200

xmlrpc-c (1.16.33-3.1) unstable; urgency=low

  * Non-maintainer upload with OK from Maintainer.
  * d/patches/636542_FTBFS_curl: New, stop using cURL headers that
    were long deprecated and have been removed by now. (Closes: #636542)
  * Fix some lintian errors and warnings.
  * Make libxmlrpc-c3-dev arch:all, it's transitional and empty.

 -- Thorsten Glaser <tg@mirbsd.de>  Thu, 15 Sep 2011 18:14:38 +0000

xmlrpc-c (1.16.33-3) unstable; urgency=low

  * Add patch to fix FTBFS on hurd-i386.
    Thanks to Pino Toscano <toscano.pino@tiscali.it> (Closes: #614937)
  * ACK NMU for #560942.  Thanks, Moritz.
  * Replace Suggests: xml-rpc-api2cpp/xml-rpc-api2txt with xmlrpc-api-utils
  * Update gbp.conf to indicate we are using pristine-tar in the git repo

 -- Sean Finney <seanius@debian.org>  Thu, 07 Jul 2011 22:09:34 +0200

xmlrpc-c (1.16.33-2) unstable; urgency=low

  * Revert "Remove kfreebsd patch, incorporated upstream"
  * Update kfreebsd patch for latest upstream build system changes.

 -- Sean Finney <seanius@debian.org>  Sun, 20 Feb 2011 12:51:45 +0100

xmlrpc-c (1.16.33-1) unstable; urgency=low

  * New upstream release! (Closes: #558972, #524540, #575560).
  * Fix FTBFS on kfreebsd-* (originally included a new patch, but
    was subsequently fixed upstream, it seems) (Closes: #565785).
  * Split out library packages to deal with the SONAME changes.
    - libxmlrpc-core-c3: C libraries (unchanged SONAME)
    - libxmlrpc-c++4 (changed SONAME)
  * Include transitional packages for the dev packages to make a smooth
    path for binNMU's, but not the lib packages themselves, to avoid
    breaking existing packages.
  * Install files with --fail-missing in debian/rules to keep a closer eye
    on SONAME changes in the future.
  * Move the separately packaged api utilities into a single package
  * Move homepage information to policy-recommended Homepage field
  * Update Standards-Version to 3.9.1
  * Set build-dependency on debhelper to >= 5, as it should be

 -- Sean Finney <seanius@debian.org>  Sat, 19 Feb 2011 18:03:30 +0000

xmlrpc-c (1.16.07-1) unstable; urgency=low

  * New upstream release.

 -- Sean Finney <seanius@debian.org>  Thu, 19 Feb 2009 15:46:23 +0100

xmlrpc-c (1.06.27-2) unstable; urgency=low

  * Include everything from the examples directory to the -dev package, making
    sure it's not compressed, and that any built objects are cleaned.
    (closes: #479988).
  * Update to Standards-Version 3.8.0 (no changes required).

 -- Sean Finney <seanius@debian.org>  Thu, 10 Jul 2008 19:38:16 +0200

xmlrpc-c (1.06.27-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Fix CVE-2009-3560 and CVE-2009-3720 (Closes: #560942)

 -- Moritz Muehlenhoff <jmm@debian.org>  Wed, 28 Jul 2010 22:18:54 -0400

xmlrpc-c (1.06.27-1) unstable; urgency=low

  * New upstream release
  * FTBFS fix for macro "curl_easy_setopt", thanks to Adam Sloboda and Peter
    Green for the suggestions of the easy fix (closes: #477016).

 -- Sean Finney <seanius@debian.org>  Thu, 15 May 2008 22:27:17 +0200

xmlrpc-c (1.06.25-2) unstable; urgency=low

  * disable building of libwww client, removing all dependencies on the
    libwww packages, as they are buggy and will be removed from debian.
    thanks to Regis Boudin for the patch (closes: #458775).
  * start build-depending on (and using) quilt for add-on patch management.
  * new patch: old-libtool, fixing FTBFS on kfreebsd-gnu.  thanks to
    Petr Salinger (closes: #466054).

 -- Sean Finney <seanius@debian.org>  Thu, 20 Mar 2008 08:35:02 +0100

xmlrpc-c (1.06.25-1) unstable; urgency=low

  * New Upstream Version
  * Bump Standards-Version to 3.7.3
  * debian/rules cleanup, add autotools-dev to build-deps.

 -- Sean Finney <seanius@debian.org>  Thu, 13 Mar 2008 23:56:29 +0100

xmlrpc-c (1.06.21-3) unstable; urgency=low

  * re-upload due to ftp-master outage.
  * change build-deps for curl to libcurl4-openssl-dev | libcurl3-openssl-dev,
    to facilitate easier backporting to etch.

 -- Sean Finney <seanius@debian.org>  Mon, 28 Jan 2008 21:57:45 +0100

xmlrpc-c (1.06.21-2) unstable; urgency=low

  * change build-deps for curl to libcurl4-openssl-dev | libcurl3-openssl-dev,
    to facilitate easier backporting to etch.

 -- Sean Finney <seanius@debian.org>  Sun, 04 Nov 2007 11:59:26 +0100

xmlrpc-c (1.06.21-1) unstable; urgency=low

  * new upstream release
  * Wasn't building with curl support, needed to add libcurl4-openssl-dev
    to the list of build-dependencies.  Thanks to Bas van Sisseren for
    catching this (closes: #309954).

 -- Sean Finney <seanius@debian.org>  Sat, 03 Nov 2007 18:46:58 +0100

xmlrpc-c (1.06.18-1) unstable; urgency=low

  * new upstream release (closes: #317979).
    - now builds with ssl support (closes: #309954).
    - now ships with a libxmlrpc_cpp.so (closes: #433195).
  * patched various source files to explictly include headers that
    will cause an FTBFS for gcc >= 4.3 (closes: #417773).
  * include a build target (closes: #395797).
  * add conflicts against libxmlrpc++-dev in this dev package, due to
    newly introduced file conflicts.
  * lintian fixes:
    - update Standards-Version to 3.7.2
    - update clean target to conditionally run, but we still have to ignore
      errors because it doesn't always work.
    - add lintian ignore rule for the soname difference, (foo-c3 vs foo3).
  * in the interest of credit where it is due, the packaging is entirely
    based on the ubuntu/1.06.17-0ubuntu4 package.

 -- Sean Finney <seanius@debian.org>  Fri, 28 Sep 2007 21:57:43 +0200

xmlrpc-c (1.06.17-0ubuntu4) gutsy; urgency=low

  * libxmlrpc-c3-dev:
    -> Fix header file transition link: the legacy name for
      /usr/include/xmlrpc-c/server_abyss.h is xmlrpc_abyss.h, not
      xmlrpc_server_abyss.h (LP: #134529).
    -> Ship xmlrpc and its documentation (LP: #134985).

 -- Jeremie Corbier <jcorbier@ubuntu.com>  Mon, 27 Aug 2007 15:44:57 -0700

xmlrpc-c (1.06.17-0ubuntu3) gutsy; urgency=low

  * The WTF release.
  * Properly install files, including manpages (LP: #133766).

 -- Jeremie Corbier <jcorbier@ubuntu.com>  Tue, 21 Aug 2007 13:51:25 -0700

xmlrpc-c (1.06.17-0ubuntu2) gutsy; urgency=low

  * debian/rules: Add $(MAKE) CADD=-fPIC for AMD64 FTBFS

 -- Barry deFreese <bddebian@comcast.net>  Mon, 13 Aug 2007 10:49:33 -0400

xmlrpc-c (1.06.17-0ubuntu1) gutsy; urgency=low

  * New upstream version.  (LP: #61682)
  * debian/control
    - Updated Maintainer value to match Debian-Maintainer-Field Spec.
    - Changed ${Source-Version} to ${binary:Version} (safely binNMUable).
    - Fixed typo (description-synopsis-might-not-be-phrased-properly).
  * Use of debian/compat instead of DH_COMPAT.
    - debian/compat: Updated.
    - debian/rules: Removed 'export DH_COMPAT=3'
  * debian/rules
    - Updated to work with new upstream version.
  * debian/libxmlrpc-c3-dev.docs
    - Updated documentation available.

 -- Miguel Ruiz <mruiz@ubuntu.com>  Sun, 12 Aug 2007 19:17:06 -0400

xmlrpc-c (0.9.10-4) unstable; urgency=low

  * Fixed timestamps on build files (closes: Bug#229456)

 -- Chris Leishman <masklin@debian.org>  Wed, 17 Mar 2004 21:50:18 +1100

xmlrpc-c (0.9.10-3) unstable; urgency=low

  * Updated libtool (closes: Bug#201940)

 -- Chris Leishman <masklin@debian.org>  Mon,  8 Dec 2003 16:47:30 +1100

xmlrpc-c (0.9.10-2) unstable; urgency=low

  * g++ 3.2 compatibility fixes (closes: Bug#177741)

 -- Chris Leishman <masklin@debian.org>  Fri, 4 Mar 2003 11:37:12 +0300

xmlrpc-c (0.9.10-1) unstable; urgency=low

  * Upstream version 0.9.10
  * Updated config.sub and config.guess (closes: Bug#166820)

 -- Chris Leishman <masklin@debian.org>  Fri, 17 Jan 2003 01:01:22 +0300

xmlrpc-c (0.9.9-5) unstable; urgency=low

  * Added conflict against libxmlrpc-c0 (closes: Bug#155050)
  * Installed overview.txt into /usr/share/doc (closes: Bug#153223)

 -- Chris Leishman <masklin@debian.org>  Mon, 5 Aug 2002 14:51:22 +0300

xmlrpc-c (0.9.9-4) unstable; urgency=low

  * Changed package names to libxmlrpc-c3(-dev) (closes: Bug#147739)
  * Added depend on libwww-dev to libxmlrpc-c0-dev (closes: Bug#147353)

 -- Chris Leishman <masklin@debian.org>  Thu, 30 May 2002 10:24:31 +1000

xmlrpc-c (0.9.9-3) unstable; urgency=low

  * Updates for gcc-3.0 compatibility (closes: Bug#111392)
  * Updated config.guess & friends.

 -- Chris Leishman <masklin@debian.org>  Tue, 25 Sep 2001 02:01:31 -0700

xmlrpc-c (0.9.9-2) unstable; urgency=low

  * Fixed some issues in xmlrpc.h

 -- Chris Leishman <masklin@debian.org>  Wed, 29 Aug 2001 09:57:52 -0700

xmlrpc-c (0.9.9-1) unstable; urgency=low

  * Initial release.

 -- Chris Leishman <masklin@debian.org>  Thu, 16 Aug 2001 13:50:52 +1000

Local variables:
mode: debian-changelog
End:
